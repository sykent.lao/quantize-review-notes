# 自动复盘 2023-05-01
## 20 日板块排名
|   排名 | code   | name       | change   | amount   |   rps05 |   rps10 |   rps20 |   rps50 |   rps120 |   rps250 | volume      |
|-------:|:-------|:-----------|:---------|:---------|--------:|--------:|--------:|--------:|---------:|---------:|:------------|
|      0 | BK0903 | 云游戏     | 8.44%    | 314.89亿 |     100 |      99 |     100 |     100 |      100 |      100 | 2780.44万手 |
|      1 | BK1046 | 游戏       | 10.46%   | 429.60亿 |     100 |     100 |     100 |     100 |      100 |      100 | 3795.18万手 |
|      2 | BK0729 | 船舶制造   | 1.10%    | 126.27亿 |      99 |     100 |      99 |      99 |       92 |       97 | 1056.84万手 |
|      3 | BK0486 | 文化传媒   | 8.28%    | 627.15亿 |      99 |      97 |      99 |      99 |       99 |       96 | 5946.34万手 |
|      4 | BK1134 | 算力概念   | 4.54%    | 588.36亿 |      14 |      91 |      98 |       0 |        0 |        0 | 1916.07万手 |
|      5 | BK0505 | 中字头     | 2.48%    | 851.99亿 |      82 |      96 |      98 |      97 |       96 |       91 | 8039.04万手 |
|      6 | BK0941 | 疫苗冷链   | 0.86%    | 66.71亿  |      96 |      99 |      97 |      91 |       86 |       80 | 561.52万手  |
|      7 | BK0847 | 影视概念   | 6.21%    | 286.52亿 |      76 |      44 |      97 |      97 |       97 |       77 | 3247.74万手 |
|      8 | BK1044 | 生物制品   | 0.77%    | 83.96亿  |      40 |      47 |      97 |      76 |       69 |       71 | 398.30万手  |
|      9 | BK1040 | 中药       | 0.40%    | 191.49亿 |      97 |      94 |      96 |      86 |       80 |       82 | 1266.27万手 |
|     10 | BK0425 | 工程建设   | 2.71%    | 294.44亿 |      90 |      93 |      96 |      90 |       86 |       51 | 4088.37万手 |
|     11 | BK0475 | 银行       | 0.62%    | 237.13亿 |      87 |      98 |      95 |      87 |       78 |       11 | 3342.43万手 |
|     12 | BK1106 | 创新药     | 0.76%    | 130.81亿 |      62 |      59 |      95 |      81 |       70 |        0 | 686.59万手  |
|     13 | BK0606 | 油气设服   | 1.87%    | 113.58亿 |      91 |      95 |      94 |      81 |       67 |       49 | 1377.80万手 |
|     14 | BK0662 | 在线教育   | 6.29%    | 856.46亿 |      98 |      77 |      94 |      98 |       98 |       97 | 6400.06万手 |
|     15 | BK0421 | 铁路公路   | 2.15%    | 27.93亿  |      98 |      98 |      94 |      85 |       85 |       59 | 503.42万手  |
|     16 | BK0615 | 中药概念   | 0.53%    | 328.05亿 |      94 |      87 |      93 |      78 |       63 |       60 | 2364.02万手 |
|     17 | BK0450 | 航运港口   | 1.93%    | 58.41亿  |      72 |      97 |      93 |      80 |       59 |       53 | 1110.57万手 |
|     18 | BK0975 | 磁悬浮概念 | 2.46%    | 98.70亿  |      63 |      88 |      92 |      88 |       84 |       73 | 1262.28万手 |
|     19 | BK1102 | 空气能热泵 | 1.28%    | 85.98亿  |      78 |      97 |      92 |      82 |       78 |        0 | 548.36万手  |